﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;

using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Models.ViewModels;
using DWP.InspiringDigital.Services;
using DWP.InspiringDigital.Filters;

namespace DWP.InspiringDigital.Controllers
{   
    public class HomeController : Controller
    {
        IUserProfileService _userProfileService;
        IEmailService _emailService;

        
        public HomeController(IUserProfileService userProfileService, IEmailService emailService)
        {
            _userProfileService = userProfileService;
            _emailService = emailService;
        }

        public ActionResult Index()
        {

            if (!IsCapableBrowser())
            {
                return RedirectToAction("BrowserWarning");
            }
            /*
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Activity");
            }*/

            return View();
        }

        [HttpPost]
        public ActionResult Index(ActivityType activityType)
        {
            if (ModelState.IsValid)
            {
                _userProfileService.SetActivitiesChosen(activityType);
                return RedirectToAction( "index","activity", new { });
            }

            return View();
        }

        public ActionResult BrowserWarning() {

            return View();
        }


      


        public float GetInternetExplorerVersion()
        {
            // Returns the version of Internet Explorer or a -1
            // (indicating the use of another browser).
            float versionNumber = -1;
            HttpBrowserCapabilitiesBase browser = Request.Browser;
            if (browser.Browser == "IE")
                versionNumber = (float)(browser.MajorVersion + browser.MinorVersion);
            return versionNumber;
        }

        public bool IsCapableBrowser()
        {
            double versionNumber = GetInternetExplorerVersion();
            if (versionNumber != -1)
            {
                if (versionNumber >= 8.0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
    }
}
