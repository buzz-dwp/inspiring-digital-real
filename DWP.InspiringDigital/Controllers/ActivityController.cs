﻿using DWP.InspiringDigital.Dal;
using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Models.ViewModels;
using DWP.InspiringDigital.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DWP.InspiringDigital.Controllers
{
    public class ActivityController : Controller
    {
        private readonly IGenricRepository<ProgressRecord> _progressRecordRepository;
        private readonly IUserProfileService _userProfileService;
        private readonly IActivityService _activityService;

        public ActivityController(IActivityService activityService, IUserProfileService userProfileService, IGenricRepository<ProgressRecord> progressRecordRepository)
        {
            _activityService = activityService;
            _userProfileService = userProfileService;
            _progressRecordRepository = progressRecordRepository;
        }

        public ActionResult Index()
        {
            var activities = _activityService.GetActivities();
            var activitiesViewModel = new ActivitiesViewModel
            {
                GettingStartedActivities = activities.Where(x => x.Type == Models.ActivityType.GettingStarted).Select(x => new ActivityViewModel(x, _userProfileService.GetActivityComplete(x), _userProfileService.GetActivityChosen(x))).ToList(),
                JobHuntingActivities = activities.Where(x => x.Type == Models.ActivityType.JobHunting).Select(x => new ActivityViewModel(x, _userProfileService.GetActivityComplete(x), _userProfileService.GetActivityChosen(x))).ToList()
            };

            return View(activitiesViewModel);
        }

        public ActionResult GetStarted(int[] Chosen)
        {
            var progressRecords = _userProfileService.GetCurrentProfile().ProgressRecords;
            foreach (var progressRecord in progressRecords)
            {
                progressRecord.IsChosen = Chosen == null ? false : Chosen.Contains(progressRecord.ActivityId);
                _progressRecordRepository.Update(progressRecord);
            }


            var activities = _activityService.GetActivities();
            var activitiesViewModel = new ActivitiesViewModel
            {
                GettingStartedActivities = activities.Where(x => x.Type == Models.ActivityType.GettingStarted).Select(x => new ActivityViewModel(x, _userProfileService.GetActivityComplete(x), _userProfileService.GetActivityChosen(x))).ToList(),
                JobHuntingActivities = activities.Where(x => x.Type == Models.ActivityType.JobHunting).Select(x => new ActivityViewModel(x, _userProfileService.GetActivityComplete(x), _userProfileService.GetActivityChosen(x))).ToList()
            };

            return RedirectToAction("NextChosenActivity");
        }

        public ActionResult Repeat(int id)
        {
            var activity = _activityService.GetActivity(id);
            _userProfileService.SetActivityComplete(activity, false);
            _userProfileService.UpdateProgressForActivity(activity, 1);

            return RedirectToAction("Index", activity.Controller);
        }

        public ActionResult NextChosenActivity()
        {
            var chosenProgressRecord = _userProfileService.GetCurrentProfile().ProgressRecords.FirstOrDefault(x => x.IsChosen && !x.IsComplete);
            if (chosenProgressRecord == null)
                return RedirectToAction("Index");

            return RedirectToAction("Index", chosenProgressRecord.Activity.Controller);
        }
    }
}
