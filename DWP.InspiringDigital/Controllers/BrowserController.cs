﻿using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DWP.InspiringDigital.Controllers
{
    public class BrowserController : Controller
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IActivityService _activityService;

        private readonly Activity _browserActivity;

        public BrowserController(IUserProfileService userProfileService, IActivityService activityService)
        {
            _userProfileService = userProfileService;
            _activityService = activityService;

            _browserActivity = _activityService.GetActivity("Browser");
        }

        public ActionResult Index()
        {
            var userProfile = _userProfileService.GetCurrentProfile();

            var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == _browserActivity.Id).FirstOrDefault();
            if (progressRecord == null)
            {
                progressRecord = _userProfileService.UpdateProgressForActivity(_browserActivity, 1);
            }

            int currentStep = progressRecord.Progress;
            ViewBag.NumberOfSteps = 5;
            ViewBag.CurrentStep = currentStep;
            ViewBag.Title = _browserActivity.Title;

            if (progressRecord.IsComplete)
            {
                return View("Summary");
            }

            return View("Browser" + currentStep);

        }

        public ActionResult StepComplete(int id)
        {
            id++;
            var userProfile = _userProfileService.GetCurrentProfile();
            var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == _browserActivity.Id).FirstOrDefault();
            if (progressRecord != null)
            {
                progressRecord = _userProfileService.UpdateProgressForActivity(_browserActivity, id);
            }
            return Redirect("/Browser");
        }

        public ActionResult NewTab()
        {
            return View();
        }

        public ActionResult Summary()
        {
            _userProfileService.SetActivityComplete(_browserActivity);
            ViewBag.Title = _browserActivity.Title;
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.NumberOfSteps = 5;
            ViewBag.CurrentStep = 5;
            ViewBag.Title = _browserActivity.Title;
            ViewBag.Action = "Register";

            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel registerModel)
        {
            if (ModelState.IsValid) {
                ViewBag.Title = _browserActivity.Title;

                string error = string.Empty;
                _userProfileService.SetActivityComplete(_browserActivity);

                if (string.IsNullOrEmpty(error))
                {
                    _userProfileService.MigrateCurrent(registerModel, out error);
                    return View("Registered");
                }
                else
                {
                    _userProfileService.SetActivityComplete(_browserActivity, false);
                    ModelState.AddModelError("", error);
                }

                return View("Registered");
            }

            return Redirect("/Browser/Register");
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    