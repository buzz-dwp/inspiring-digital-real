﻿using DWP.InspiringDigital.Filters;
using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Models.ViewModels;
using DWP.InspiringDigital.Services;
using DWP.InspiringDigital.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using OpenPop.Pop3;
using System.Collections.Concurrent;

namespace DWP.InspiringDigital.Controllers
{
    public class EmailController : Controller
    {

        private string _defaultViewName = "email-{0}";
        private int _numberOfSteps = 6;

        private readonly IUserProfileService _userProfileService;
        private readonly IActivityService _activityService;
        private readonly IEmailService _emailService;
        private readonly IPopService _popService;


        private readonly Activity _emailActivity;

        private static ConcurrentBag<PopMessageRecord> _messages = new ConcurrentBag<PopMessageRecord>();

        public EmailController(IUserProfileService userProfileService, IActivityService activityService, IEmailService emailService, IPopService popService)
        {
            _userProfileService = userProfileService;
            _activityService = activityService;
            _emailService = emailService;
            _popService = popService;

            _emailActivity = _activityService.GetActivity("Email");
        }


        [ImportModelStateFromTempData]
        public ActionResult Index(string substep)
        {
            var userProfile = _userProfileService.GetCurrentProfile();

            var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == _emailActivity.Id).FirstOrDefault();
            if (progressRecord == null)
            {
                progressRecord = _userProfileService.UpdateProgressForActivity(_emailActivity, 1);
            }

            int currentStep = progressRecord.Progress;

            ViewBag.NumberOfSteps = _numberOfSteps;
            ViewBag.CurrentStep = currentStep;
            ViewBag.Title = _emailActivity.Title;

            var viewName = string.Format(_defaultViewName, currentStep);

            if (!string.IsNullOrEmpty(substep))
            {
                viewName = string.Format("{0}-{1}", viewName, substep.ToLower());
            }

            return View(viewName,userProfile);
        }

        [HttpPost]
        public ActionResult NextStep(int nextstep)
        {
            if (nextstep != 0)
            {
                var userProfile = _userProfileService.GetCurrentProfile();

                var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == _emailActivity.Id).FirstOrDefault();
               
                if (progressRecord != null)
                {
                    if (nextstep > _numberOfSteps)
                    {
                        _userProfileService.SetActivityComplete(_emailActivity, true);
                        return RedirectToAction("index", "activity", new { }); 
                        }
                    else
                    {
                        _userProfileService.UpdateProgressForActivity(_emailActivity, nextstep);
                    }
                }
            }

            return Redirect("/Email");
        }


        [HttpPost]
        public ActionResult Finish(bool returnToDashboard)
        {

            var userProfile = _userProfileService.GetCurrentProfile();
            var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == _emailActivity.Id).FirstOrDefault();
            _userProfileService.SetActivityComplete(_emailActivity, true);

            if (returnToDashboard)
            {
                return RedirectToAction("index", "activity", new { });
            }
            else
            {
                return RedirectToAction(" nextchosenactivity", "activity", new { });
            }
        }

        [ExportModelStateToTempData]
        public ActionResult UpdateEmail(EmailAddressViewModel form)
        {
            if (ModelState.IsValid)
            {
                _userProfileService.UpdateEmailAddress(form);
                return SendEmail();
            }
            else
            {
                return Redirect(Request.UrlReferrer.PathAndQuery);
            }
        }

        [HttpPost]
        public JsonNetResult UpdateEmailAsync(EmailAddressViewModel form)
        {
            var ret = new JsonNetResult();

            if (ModelState.IsValid)
            {
                _userProfileService.UpdateEmailAddress(form);
            }
            else
            {
                ret.Data = new
                {
                    errors = ModelState.SelectMany(m => m.Value.Errors).Select(e => e.ErrorMessage) 
                };
            }

            return ret;
        }

        [HttpPost]
        public ActionResult SendEmail()
        {
            var userProfile = _userProfileService.GetCurrentProfile();
            EmailTestViewModel vm = new EmailTestViewModel{
                Name = userProfile.Name,
                Email = userProfile.Email
            };

            _emailService.SendEmail(Helpers.GetSettingString("Email.EmailActivity.FromAddress"), userProfile.Email, string.Empty, Helpers.GetSettingString("Email.EmailActivity.Subject"), XmlHelpers.Serialize(vm), Helpers.GetSettingString("Email.EmailActivity.Template"));

            return NextStep(2);

        }



        [HttpPost]
        public JsonNetResult CheckEmail()
        {
            var userProfile = _userProfileService.GetCurrentProfile();

            //if not service will trigger a poll and we'll check back on next call
            bool msgRecieved = _popService.CheckForEmailFrom(userProfile.Email);

            /*
            if (!msgRecieved)
            {
                _popService.SyncEmail();
                msgRecieved = _popService.CheckForEmailFrom(userProfile.Email);
            }*/
            
           return new JsonNetResult{
                    Data = new { success = msgRecieved }
                };
        }


     

    }
}
