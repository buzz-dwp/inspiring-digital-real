﻿using System.Web.Mvc;

namespace DWP.InspiringDigital.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
          name: "AdminReport",
          url: "admin/report/{reportName}/{siteProviderId}",
          defaults: new { controller = "Reports", action = "GetReport" }
      );
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

          
        }
    }
}
