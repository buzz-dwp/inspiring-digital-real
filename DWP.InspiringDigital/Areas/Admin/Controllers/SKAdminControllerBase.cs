﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using AutoMapper;
using DWP.InspiringDigital.Filters;
using System.Reflection;

namespace DWP.InspiringDigital.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public abstract class SKAdminControllerBase : Controller
    {

        protected TViewModel ConvertEntityToViewModel<TEntity,TViewModel>(TEntity entity)
        {
            Mapper.CreateMap<TEntity, TViewModel>();
            return Mapper.Map<TEntity, TViewModel>(entity);
        }

        protected void UpdateEntityWithViewModel<TViewModel, TEntity>(TViewModel viewModel, TEntity entity)
        {
            Mapper.CreateMap<TViewModel, TEntity>();
            Mapper.Map((TViewModel)viewModel, (TEntity)entity);
        }

    protected string GetActionUrl(string action, object routeValues)
    {
        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
        return u.Action(action, ControllerContext.RouteData.Values["Controller"].ToString() , routeValues);
    }

    }


}
