﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DWP.InspiringDigital.Dal;
using  DWP.InspiringDigital.App_Start;

using Ninject;
using Ninject.Web.Common;

[assembly: WebActivator.PreApplicationStartMethod(typeof(DWP.InspiringDigital.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(DWP.InspiringDigital.App_Start.NinjectWebCommon), "Stop")]

namespace DWP.InspiringDigital
{
    public static class DBConfig
    {

        public static void InitializeDBConfig() {

            var kernel = NinjectWebCommon.GetKernel();
            IRepositoryInitializer repositoryInitializer = kernel.Get<IRepositoryInitializer>();
            repositoryInitializer.Initialize();

        }
    }
}