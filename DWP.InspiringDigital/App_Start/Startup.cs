﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using DWP.InspiringDigital.Hubs;
using DWP.InspiringDigital.Services;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(DWP.InspiringDigital.App_Start.Startup))]

namespace DWP.InspiringDigital.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var kernel = NinjectWebCommon.GetKernel();

            var resolver = new NinjectSignalRDependencyResolver(kernel);

            kernel.Bind(typeof(IHubConnectionContext<dynamic>)).ToMethod(context =>
                   resolver.Resolve<IConnectionManager>().GetHubContext<ActivityHub>().Clients
                    ).WhenInjectedInto<IUserProfileService>();


            var config = new HubConfiguration();
            config.Resolver = resolver;
            ConfigureSignalR(app, config);
        }


        public static void ConfigureSignalR(IAppBuilder app, HubConfiguration config)
        {
            app.MapSignalR(config);
        }
    }
}
