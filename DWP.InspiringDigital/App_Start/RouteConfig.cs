﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DWP.InspiringDigital
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                           name: "BrowserNewTab",
                           url: "newtab",
                           defaults: new { controller = "Browser", action = "NewTab" }
                       );

            routes.MapRoute(
                           name: "BrowserNextStep",
                           url: "nextstep",
                           defaults: new { controller = "Browser", action = "StepComplete", id = 1 }
                       );

            routes.MapRoute(
                           name: "Browser",
                           url: "browser",
                           defaults: new { controller = "Browser", action = "Index" }
                       );

            routes.MapRoute(
                  name: "EmailAction",
                  url: "email/{action}",
                  defaults: new { controller = "Email" , action = "index"}
              );

          

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

           
        }
    }
}