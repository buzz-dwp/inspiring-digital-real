using DWP.InspiringDigital.Services;
using DWP.InspiringDigital.Dal;

[assembly: WebActivator.PreApplicationStartMethod(typeof(DWP.InspiringDigital.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(DWP.InspiringDigital.App_Start.NinjectWebCommon), "Stop")]



namespace DWP.InspiringDigital.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Microsoft.AspNet.SignalR.Hubs;
    using Microsoft.AspNet.SignalR.Infrastructure;
    using DWP.InspiringDigital.Hubs;


    public static class NinjectWebCommon 
    {

        private static IKernel _kernel = null;

        public static IKernel GetKernel()
        {
            if (_kernel == null)
            {
                CreateKernel();
            }
            return _kernel;
        }

        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            RegisterServices(kernel);

            _kernel = kernel;

            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
           // kernel.Bind(typeof(IGenricRepository<>)).To(typeof(GenericRepository<>)).WhenInjectedInto<PopPollingService>().InTransientScope();
           // kernel.Bind<IUnitOfWork>().To<InspiringDigitalDbContext>().WhenAnyAnchestorNamed("popPollingService").InTransientScope();

            kernel.Bind<IRepositoryInitializer>().To<RepositoryInitializer>().InRequestScope();
            kernel.Bind<IUserProfileService>().To<UserProfileService>().InRequestScope();
            kernel.Bind(typeof(IGenricRepository<>)).To(typeof(GenericRepository<>)).InRequestScope();
            kernel.Bind<IEmailService>().To<EmailService>().InRequestScope();
            kernel.Bind<IUnitOfWork>().To<InspiringDigitalDbContext>().InRequestScope();
            kernel.Bind<ILogService>().To<LogService>().InRequestScope();
            kernel.Bind<IActivityService>().To<ActivityService>().InRequestScope();
            kernel.Bind<IPopService>().To<PopService>().InRequestScope();
            kernel.Bind<IPopPollingService>().To<PopPollingService>().InSingletonScope();

        }


        public static T Resolve<T>()
        {
            if (_kernel == null)
            {
                CreateKernel();
            }
            return _kernel.Get<T>();
        }

        public static T Resolve<T>(string name)
        {
            if (_kernel == null)
            {
                CreateKernel();
            }

            return _kernel.Get<T>(name);
            //return kernel.Get<T>(With.Parameters.ContextVariable("name", name));
        }
    }
}
