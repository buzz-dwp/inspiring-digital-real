﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using RestSharp.Extensions;
using System.IO;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RestSharp.Deserializers
{
	public class JsonNetDeserializer : IDeserializer
	{
		public string RootElement { get; set; }
		public string Namespace { get; set; }
		public string DateFormat { get; set; }
		public CultureInfo Culture { get; set; }

		public JsonNetDeserializer ()
		{
			Culture = CultureInfo.InvariantCulture;


			//DateTime ddate  = DateTime.MinValue;
			//string json = JsonConvert.SerializeObject(ddate);



			//JObject dummy = JObject.Parse(json);
			//dummy.ToObject<DateTime>();

			//JToken dummyToken = JToken.Parse("{2013-01-01T00:00:00+00:00}");
			//dummyToken.ToObject<DateTime>();

		}

		public T Deserialize<T>(IRestResponse response)
		{
			if (string.IsNullOrEmpty(response.Content))
			{
				return default(T);
			}

			JObject jroot = JObject.Parse(response.Content);

			if(string.IsNullOrEmpty(RootElement))
			{
				return jroot.ToObject<T>();
			}
			else
			{
				return jroot.Properties().FirstOrDefault(p => p.Name == RootElement).Value.ToObject<T>();
			}
		}

		private object FindRoot(string content)
		{
            throw new NotImplementedException();
		}
	}
}