﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DWP.InspiringDigital.Models;

namespace DWP.InspiringDigital.Services
{
    public interface IEmailService
    {
        void SendEmail(string fromAddress, string toAddress, string ccAddresses, string subject, string xml, string xsltPath);
        void SendEmail(string fromAddress, string toAddress, string ccAddresses, string subject, string body);
        IEnumerable<Email> GetEmailLog(DateTime startDate, DateTime endDate);
        void ProcessUnsentEmails();
    }
}
