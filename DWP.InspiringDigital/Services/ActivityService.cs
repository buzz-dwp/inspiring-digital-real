﻿using DWP.InspiringDigital.Dal;
using DWP.InspiringDigital.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Services
{
    public class ActivityService : IActivityService
    {
        IGenricRepository<Activity> _activityRepository;

        public ActivityService(IGenricRepository<Activity> activityRepository)
        {
            _activityRepository = activityRepository;
        }

        public IList<Activity> GetActivities()
        {
            return _activityRepository.Get().ToList();
        }

        public IList<Activity> GetActivities(ActivityType type)
        {
            return _activityRepository.Get(x => x.Type == type).ToList();
        }

        public Activity GetActivity(int id)
        {
            return _activityRepository.GetSingle(x => x.Id == id);
        }

        public Activity GetActivity(string name)
        {
            return _activityRepository.GetSingle(x => x.Name == name);
        }
    }
}