﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DWP.InspiringDigital.Dal;
using DWP.InspiringDigital.Models;

namespace DWP.InspiringDigital.Services
{
    public interface ILogService
    {
        void LogError(string src, string msg, Exception ex);
        void LogMessage(string src, string shortMsg, string fullMsg);
    }

    public class LogService :ILogService
    {
        private readonly GenericRepository<Log> _logRepository;

        public LogService(GenericRepository<Log> logRepository)
        {
            _logRepository = logRepository;
        }

        public void LogError(string src,string msg, Exception ex)
        {
            _logRepository.Add(new Log
            {
                Source = src,
                ShortMessage = msg,
                FullMessage = ex.ToString(),
                EventDate = DateTime.UtcNow
            });
        }

        public void LogMessage(string src, string shortMsg, string fullMsg)
        {
            _logRepository.Add(new Log
            {
                Source = src,
                ShortMessage = shortMsg,
                FullMessage = fullMsg,
                EventDate = DateTime.UtcNow
            });
        }
    }
}