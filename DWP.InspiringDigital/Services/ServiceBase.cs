﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DWP.InspiringDigital.Models;

namespace DWP.InspiringDigital.Services
{
    public class ServiceBase
    {

        protected TViewModel ConvertEntityToViewModel<TEntity, TViewModel>(TEntity entity)
        {
            Mapper.CreateMap<TEntity, TViewModel>();
            return Mapper.Map<TEntity, TViewModel>(entity);
        }

        protected void UpdateEntityWithViewModel<TViewModel, TEntity>(TViewModel viewModel, TEntity entity)
        {
            Mapper.CreateMap<TViewModel, TEntity>();
            Mapper.Map((TViewModel)viewModel, (TEntity)entity);
        }

        protected void UpdateEntityWithViewModelExcludeId<TViewModel, TEntity>(TViewModel viewModel, TEntity entity) where TEntity: IEntity
        {
       
                Mapper.CreateMap<TViewModel, TEntity>().ForMember(ent => ent.Id, opt => opt.Ignore());
           

            Mapper.Map((TViewModel)viewModel, (TEntity)entity);
        }
    }
}