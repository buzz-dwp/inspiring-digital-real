﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;

using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Models.ViewModels;

using DWP.InspiringDigital.Dal;
using DWP.InspiringDigital.Filters;
using DWP.InspiringDigital.Utility;

namespace DWP.InspiringDigital.Services
{
    public class UserProfileService : ServiceBase, IUserProfileService
    {

        private readonly IGenricRepository<ProgressRecord> _progressRecordRepository;
        private readonly IGenricRepository<UserProfile> _userProfileRepository;
        private readonly IGenricRepository<User> _userRepository;
        private readonly IActivityService _activityService;

        private string _includeProperties = "ProgressRecords,ProgressRecords.Activity";

        private readonly IEmailService _emailService;

        public UserProfileService(
            IGenricRepository<UserProfile> userProfileRepository,
            IEmailService emailService,
            IGenricRepository<User> userRepository,
            IGenricRepository<ProgressRecord> progressRecordRepository,
            IActivityService activityService
            )
        {
            _progressRecordRepository = progressRecordRepository;
            _userProfileRepository = userProfileRepository;
            _emailService = emailService;
            _userRepository = userRepository;
            _activityService = activityService;
        }


        public IEnumerable<UserProfile> GetUserProfiles(int siteProviderId, string includeProperties = null)
        {

            if (includeProperties != null)
            {
                includeProperties = _includeProperties + "," + includeProperties;
            }
            else
            {
                includeProperties = _includeProperties ;
            }

            return _userProfileRepository.Get(up => up.SiteProviderId == siteProviderId && up.ProfileType != ProfileType.Registering , includeProperties: includeProperties);
        }

        public UserProfile GetUserProfile(string UserName)
        {
            return _userProfileRepository.Get(u => u.UserName == UserName, includeProperties: _includeProperties).SingleOrDefault();
        }

        public UserProfile GetCurrentProfile(string includeProperties = null)
        {
            //hard code site provider for now just for simplicity as we only have one site
            //if needed we should get it based on url

            int siteProviderId = 1;

            string include = _includeProperties;
            if (includeProperties != null)
            {
                include += string.Format(",{0}",includeProperties);
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return _userProfileRepository.GetSingle(u => u.UserName == HttpContext.Current.User.Identity.Name, includeProperties: include);
            }
            else 
            {
                UserProfile ret = _userProfileRepository.GetSingle(u => u.UserName == HttpContext.Current.Request.AnonymousID, includeProperties: include);
                
                if (ret == null)
                {
                    
                    ret = new UserProfile
                    {
                        UserName = HttpContext.Current.Request.AnonymousID,
                        SiteProviderId = siteProviderId,
                        ProfileType = ProfileType.Registering,
                        ProgressRecords = new List<ProgressRecord>()
                    };

                    _userProfileRepository.Add(ret);
                }

              return ret;
            } 
        }

        public void SetActivityComplete(Activity activity, bool complete = true)
        {
            var progressRecord = GetProgressForActivity(activity);
            progressRecord.IsComplete = complete;
            _progressRecordRepository.Update(progressRecord);
        }
        
        public bool GetActivityComplete(Activity activity)
        {
            return GetProgressForActivity(activity).IsComplete;
        }

        public void SetActivitiesChosen(ActivityType type)
        {
            var activities = _activityService.GetActivities(type);
            foreach (var activity in activities)
            {
                SetActivityChosen(activity);
            }
        }

        public void SetActivityChosen(Activity activity, bool chosen = true)
        {
            var progressRecord = GetProgressForActivity(activity);
            progressRecord.IsChosen = chosen;
            _progressRecordRepository.Update(progressRecord);
        }

        public bool GetActivityChosen(Activity activity)
        {
            return GetProgressForActivity(activity).IsChosen;
        }



        public ProgressRecord UpdateProgressForActivity(Activity activity, int progress)
        {
            var userProfile = GetCurrentProfile();
            var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == activity.Id).FirstOrDefault();
            if (progressRecord == null)
            {
                progressRecord = new ProgressRecord
                {
                    UserProfileId = userProfile.Id,
                    ActivityId = activity.Id
                };
                _progressRecordRepository.Add(progressRecord);
            }
            progressRecord.Progress = progress;
            _progressRecordRepository.Update(progressRecord);
            return progressRecord;
        }

        public ProgressRecord GetProgressForActivity(Activity activity)
        {
            var userProfile = GetCurrentProfile();
            var progressRecord = userProfile.ProgressRecords.Where(x => x.ActivityId == activity.Id).FirstOrDefault();
            if (progressRecord == null)
            {
                progressRecord = new ProgressRecord
                {
                    UserProfileId = userProfile.Id,
                    ActivityId = activity.Id,
                    Progress = 1
                };
                _progressRecordRepository.Add(progressRecord);
            }
            return progressRecord;
        }

        public string GetSignalRId()
        {
            var userProfile = GetCurrentProfile();
            return userProfile.SignalRId;
        }

        public void SetSignalRId(string id)
        {
            var userProfile = GetCurrentProfile();
            userProfile.SignalRId = id;
            _userProfileRepository.Update(userProfile);
        }










        //public UserProfile UpdateBoostStatement(BoostViewModel form)
        //{
        //    UserProfile currentProfile = GetCurrentProfile();
        //    UpdateEntityWithViewModel(form, currentProfile);
        //    _userProfileRepository.Update(currentProfile);
        //    return currentProfile;
        //}

        public void MigrateCurrent(RegisterModel form, out string error)
        {
            try
            {
                string anonymousUserId = string.Empty; ;

                if (HttpContext.Current.User.IsInRole("AnonUser"))
                {
                    //we are migrating and anonymous user and so need to delete their
                    //old user if the migration is successful
                    anonymousUserId = HttpContext.Current.User.Identity.Name;
                }

                UserProfile currentProfile = GetCurrentProfile();

                WebSecurity.CreateUserAndAccount(form.UserName, form.Password, new { CreateDate = DateTime.UtcNow, ModifiedDate = DateTime.UtcNow, CreateUser = "System", IsAnonymous = false });
                WebSecurity.Login(form.UserName, form.Password);

                if (!Roles.RoleExists("EndUser"))
                {
                    Roles.CreateRole("EndUser");
                }

                Roles.AddUserToRole(form.UserName, "EndUser");

                currentProfile.UserName = form.UserName;
                currentProfile.ProfileType = ProfileType.User;

                //hard code this for now just for simplicity as we only have one site
                //currentProfile.SiteProviderId = 1;
                _userProfileRepository.Update(currentProfile);

                if (!string.IsNullOrEmpty(anonymousUserId))
                {
                    User oldUser = _userRepository.GetSingle(u => u.UserName == anonymousUserId);

                    if (oldUser != null)
                    {
                        _userRepository.Delete(oldUser);
                    }
                }

                error = string.Empty;
            }
            catch (MembershipCreateUserException e)
            {
                error = ErrorCodeToString(e.StatusCode);
            }
        }

        public void MigrateCurrentAsAnonymous(out string error)
        {
            try
            {
                string anonId = HttpContext.Current.Request.AnonymousID;
                UserProfile currentProfile = GetCurrentProfile();

                WebSecurity.CreateUserAndAccount(anonId, anonId, new { CreateDate = DateTime.UtcNow, ModifiedDate = DateTime.UtcNow, CreateUser = "System", IsAnonymous = true });
                WebSecurity.Login(anonId, anonId, false);

                if (!Roles.RoleExists("AnonUser"))
                {
                    Roles.CreateRole("AnonUser");
                }

                Roles.AddUserToRole(anonId, "AnonUser");

               
                //this is already the case but leave it for clarity
                currentProfile.UserName = anonId;

                currentProfile.ProfileType = ProfileType.AnonymousUser;
                //hard code this for now just for simplicity as we only have one site
                //currentProfile.SiteProviderId = 1;
                _userProfileRepository.Update(currentProfile);

                error = string.Empty;
            }
            catch (MembershipCreateUserException e)
            {
                error = ErrorCodeToString(e.StatusCode);
            }
        }

   
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Snap! Another hero has already nabbed that username. Try another one.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public void AddFavourite(int supportProviderId, out string error)
        {
            error = string.Empty;

            UserProfile user = GetCurrentProfile();

        }


        public void UpdateSupportCategories(int[] supportCategoryIds)
        {
            UserProfile user = GetCurrentProfile();
            

            _userProfileRepository.Update(user);
            
        }

        public void SendForgottenPasswordEmail(string username, string resetUrl) {

            UserProfile user = GetUserProfile(username);

            if (user != null)
            {
                var token = WebSecurity.GeneratePasswordResetToken(username);
                resetUrl = string.Format("http://{0}{1}?token={2}", HttpContext.Current.Request.Url.Host,resetUrl,token);

                var emailVm = new ForgottenPasswordEmailViewModel
                {
                     Email = username,
                 //     Name = user.Name,
                       ResetUrl = resetUrl
                };

                _emailService.SendEmail(Utility.Helpers.GetSettingString("Email.FromAddress"), username, string.Empty, "Your  password", Utility.XmlHelpers.Serialize(emailVm), Utility.Helpers.GetSettingString("Email.ForgottenPassword.Template"));
            }
        }

        public void ResetPassword(PassworResetModel form, out string error)
        {
            try
            {
                if (WebSecurity.ResetPassword(form.Token, form.NewPassword))
                {
                    error = string.Empty;
                }
                else
                {
                    error = "Failed to reset password";
                }


            }
            catch(Exception ex) {
                error = ex.Message;
            }

            return;
        }

        public void UpdateEmailAddress(EmailAddressViewModel form)
        {
            var userProfile = GetCurrentProfile();
            userProfile.Email = form.Email;
            _userProfileRepository.Update(userProfile);
        }

    }
}