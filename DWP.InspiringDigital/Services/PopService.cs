﻿using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Utility;
using OpenPop.Pop3;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Threading;

using DWP.InspiringDigital.Dal;
using OpenPop.Mime;
using OpenPop.Pop3.Exceptions;

namespace DWP.InspiringDigital.Services
{
    public interface IPopService
    {
        bool CheckForEmailFrom(string fromAddress);
       // void SyncEmail();
    }

    public class PopService : IPopService
    {
        private static ConcurrentBag<PopMessageRecord> _messages = new ConcurrentBag<PopMessageRecord>();

        private readonly IGenricRepository<PopMessageRecord> _popMessageRepository;
        private readonly IPopPollingService _popPollingService;

        public PopService(IGenricRepository<PopMessageRecord> popMessageRepository, IPopPollingService popPollingService)
        {
            _popMessageRepository = popMessageRepository;
            _popPollingService = popPollingService;
        }

        public bool CheckForEmailFrom(string fromAddress)
        {

            var hasMessage = (_popMessageRepository.Get(m => m.From == fromAddress).Count() > 0) ;

            //we'll poll the mailbox but not wait for it as we'll be checking again in a minute
            if (!hasMessage)
            {
                ThreadPool.QueueUserWorkItem(a => _popPollingService.SyncEmail()); 
            }

            return hasMessage;
        }


    }

    public interface IPopPollingService
    {
        void SyncEmail();
    }

    public class PopPollingService : IPopPollingService
    {
   
        private readonly IGenricRepository<PopMessageRecord> _popMessageRepository;

        public PopPollingService()
        {
            //we don't want a request scoped repository, we could sort this via ninject but haven't got time to faff
            IUnitOfWork context = new InspiringDigitalDbContext();
            _popMessageRepository = new GenericRepository<PopMessageRecord>(context);
        }

        public void SyncEmail()
        {

           //don't want concurrent poll calls and no need to block if already polling
           if(Monitor.TryEnter(_popMessageRepository))
            {
                try
                {
                    using (Pop3Client client = new Pop3Client())
                    {
                        var existingMsgs = _popMessageRepository.Get();

                        client.Connect(Helpers.GetSettingString("Email.EmailActivity.MailBox.Server"), 110, false, 20000,20000, null);
                        client.Authenticate(Helpers.GetSettingString("Email.EmailActivity.MailBox.User"), Helpers.GetSettingString("Email.EmailActivity.MailBox.Pass"));
                        //var msgInfos = client.GetMessageInfos();

                        Dictionary<string, string> msgDicitonary = new Dictionary<string, string>();

                        int messageCount = client.GetMessageCount();
                        for (int i = messageCount; i > 0; i--)
                        {
                            var msg = client.GetMessageHeaders(i);

                            if (!existingMsgs.Any(m => m.Id == msg.MessageId))
                            {
                                _popMessageRepository.Add(new PopMessageRecord
                                {
                                    Id = msg.MessageId,
                                    From = msg.From.Address
                                });
                            }
                            //now we've recorded the message  we ned to delete it from the mailbox
                            client.DeleteMessage(i);
                        }
                    }
                }
               catch(Exception ex){
                   var b = "x";
               }
            }
        }
    }
}