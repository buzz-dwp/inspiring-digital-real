﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Models.ViewModels;

namespace DWP.InspiringDigital.Services
{
    public interface IUserProfileService
    {

        IEnumerable<UserProfile> GetUserProfiles(int siteProviderId, string includeProperties = null);

        UserProfile GetUserProfile(string UserName);
        UserProfile GetCurrentProfile(string includeProperties = null);

        void SendForgottenPasswordEmail(string username, string resetUrl);
        void ResetPassword(PassworResetModel form,out string error);

        ProgressRecord GetProgressForActivity(Activity activity);
        ProgressRecord UpdateProgressForActivity(Activity activity, int progress);

        bool GetActivityComplete(Activity activity);
        void SetActivityComplete(Activity activity, bool complete = true);

        bool GetActivityChosen(Activity activity);
        void SetActivityChosen(Activity activity, bool chosen = true);
        void SetActivitiesChosen(ActivityType type);

        string GetSignalRId();
        void SetSignalRId(string id);

        void MigrateCurrent(RegisterModel form, out string error);
        void UpdateEmailAddress(EmailAddressViewModel form);
    }
}
