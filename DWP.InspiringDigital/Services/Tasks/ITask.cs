﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DWP.InspiringDigital.Services.Tasks
{
    /// <summary>
    /// Interface for tasks
    /// </summary>
    public partial interface ITask
    {
        /// <summary>
        /// Execute task
        /// </summary>
        /// <param name="node">Custom configuration node</param>
        void Execute(XmlNode node);
    }
}
