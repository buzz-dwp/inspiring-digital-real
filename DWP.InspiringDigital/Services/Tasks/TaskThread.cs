﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading;
using System.Xml;

namespace DWP.InspiringDigital.Services.Tasks
{
    public partial class TaskThread : IDisposable
    {
        private Timer _timer;
        private bool _disposed;
        private DateTime _started;
        private bool _isRunning;
        private Dictionary<string, Task> _tasks;
        private int _seconds;
        private List<TimeSpan> _runTimes;

        private TaskThread()
        {
            this._tasks = new Dictionary<string, Task>();
            this._seconds = 10 * 60;
        }

        internal TaskThread(XmlNode node)
        {
            this._tasks = new Dictionary<string, Task>();
            this._seconds = 10 * 60;
            this._isRunning = false;
            if ((node.Attributes["seconds"] != null) && !int.TryParse(node.Attributes["seconds"].Value, out this._seconds))
            {
                this._seconds = 10 * 60;
            }

            if (node.Attributes["runTimes"] != null && !string.IsNullOrEmpty(node.Attributes["runTimes"].ToString()))
            {
                _runTimes = new List<TimeSpan>();
                string[] runTimeStrs = node.Attributes["runTimes"].Value.Split(';');
                foreach (string runTimeStr in runTimeStrs)
                {
                    TimeSpan ts;

                    if (TimeSpan.TryParse(runTimeStr, out ts))
                        _runTimes.Add(ts);
                }
            }
        }

        private void run()
        {
            this._started = DateTime.Now;
            this._isRunning = true;
            foreach (Task task in this._tasks.Values)
            {
                task.Execute();
            }
            this._isRunning = false;
        }

        private void timerHandler(object state)
        {
            this._timer.Change(-1, -1);
            this.run();
            int _interval = this.Interval;
            this._timer.Change(_interval, _interval);
        }

        /// <summary>
        /// Disposes the instance
        /// </summary>
        public void Dispose()
        {
            if ((this._timer != null) && !this._disposed)
            {
                lock (this)
                {
                    this._timer.Dispose();
                    this._timer = null;
                    this._disposed = true;
                }
            }
        }

        /// <summary>
        /// Inits a timer
        /// </summary>
        public void InitTimer()
        {
            if (this._timer == null)
            {
                int _interval = this.Interval;
                this._timer = new Timer(new TimerCallback(this.timerHandler), null, _interval, _interval);
            }
        }

        /// <summary>
        /// Adds a task to the thread
        /// </summary>
        /// <param name="task">The task to be added</param>
        public void AddTask(Task task)
        {
            if (!this._tasks.ContainsKey(task.Name))
            {
                this._tasks.Add(task.Name, task);
            }
        }


        /// <summary>
        /// Gets the interval in seconds at which to run the tasks
        /// </summary>
        public int Seconds
        {
            get
            {
                return this._seconds;
            }
        }

        /// <summary>
        /// Get a datetime when thread has been started
        /// </summary>
        public DateTime Started
        {
            get
            {
                return this._started;
            }
        }

        /// <summary>
        /// Get a value indicating whether thread is running
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return this._isRunning;
            }
        }

        /// <summary>
        /// Get a list of tasks
        /// </summary>
        public IList<Task> Tasks
        {
            get
            {
                List<Task> list = new List<Task>();
                foreach (Task task in this._tasks.Values)
                {
                    list.Add(task);
                }
                return new ReadOnlyCollection<Task>(list);
            }
        }

        /// <summary>
        /// Gets the interval at which to run the tasks
        /// </summary>
        public int Interval
        {
            get
            {
                if (this._runTimes == null || this._runTimes.Count == 0)
                    return this._seconds * 1000;
                else
                {
                    int ret;
                    TimeSpan retPos = TimeSpan.MaxValue;
                    TimeSpan retNeg = TimeSpan.MinValue;
                    TimeSpan tsCont = new TimeSpan(0, 0, 0);
                    TimeSpan now = DateTime.Now.TimeOfDay;

                    foreach (TimeSpan ts in _runTimes)
                    {
                        if (retPos == TimeSpan.MaxValue && ts.Subtract(now) >= tsCont)
                            retPos = ts.Subtract(now);
                        else if (retNeg == TimeSpan.MinValue && ts.Subtract(now) < tsCont)
                            retNeg = ts.Subtract(now);
                        else if (ts.Subtract(now) >= tsCont && ts.Subtract(now) < retPos)
                            retPos = ts.Subtract(now);
                        else if (ts.Subtract(now) < tsCont && ts.Subtract(now) < retNeg)
                            retNeg = ts.Subtract(now);
                    }

                    //we should have a time in either pos or neg however just in case we'll fall back ot the seconds interval setting
                    if (retPos != TimeSpan.MaxValue)
                        ret = Convert.ToInt32(retPos.TotalMilliseconds);
                    else if (retNeg != TimeSpan.MinValue)
                        ret = Convert.ToInt32(new TimeSpan(24, 00, 00).TotalMilliseconds + retNeg.TotalMilliseconds);
                    else
                        ret = this._seconds * 1000;

                    return ret;
                }
            }

        }

    }
}