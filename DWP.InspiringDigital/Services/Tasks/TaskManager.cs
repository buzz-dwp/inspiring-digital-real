﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using System.Linq;

namespace DWP.InspiringDigital.Services.Tasks
{
    public partial class TaskManager
    {
        static readonly TaskManager _taskManager = new TaskManager();
        private List<TaskThread> _taskThreads = new List<TaskThread>();
        private ILogService _logService;

        private TaskManager()
        {
            _logService = DWP.InspiringDigital.App_Start.NinjectWebCommon.Resolve<ILogService>();

        }

        internal void ProcessException(Task task, Exception exception)
        {
            try
            {
                //process exception code here
            }
            catch
            {
            }
        }

        /// <summary>
        /// Initializes the task manager with the property values specified in the configuration file.
        /// </summary>
        /// <param name="configFile">Configuration file</param>
        /// <param name="nodePath">Node path</param>
        public void Initialize(string configFile, string nodePath)
        {
            XmlDocument document = new XmlDocument();
            document.Load(configFile);
            Initialize(document.SelectSingleNode(nodePath));
        }


        /// <summary>
        /// Initializes the task manager with the property values specified in the configuration file.
        /// </summary>
        /// <param name="node">Node</param>
        public void Initialize(XmlNode node)
        {
            this._taskThreads.Clear();
            foreach (XmlNode node1 in node.ChildNodes)
            {
                if (node1.Name.ToLower() == "thread")
                {
                    TaskThread taskThread = new TaskThread(node1);
                    this._taskThreads.Add(taskThread);
                    foreach (XmlNode node2 in node1.ChildNodes)
                    {
                        if (node2.Name.ToLower() == "task")
                        {
                            XmlAttribute attribute = node2.Attributes["type"];
                            Type taskType = Type.GetType(attribute.Value);
                            if (taskType != null)
                            {
                                Task task = new Task(taskType, node2);
                                taskThread.AddTask(task);
                            }
                        }
                    }
                }
            }
            int tasksCount = _taskThreads.SelectMany<TaskThread, Task>(tt => tt.Tasks).Count();
            _logService.LogMessage("TaskManager", string.Format("Initialized with {0} tasks", tasksCount), string.Empty);
        }

        /// <summary>
        /// Starts the task manager
        /// </summary>
        public void Start()
        {
            foreach (TaskThread taskThread in this._taskThreads)
            {
                taskThread.InitTimer();
            }
        }

        /// <summary>
        /// Stops the task manager
        /// </summary>
        public void Stop()
        {
            foreach (TaskThread taskThread in this._taskThreads)
            {
                taskThread.Dispose();
            }
        }

        /// <summary>
        /// Gets the task mamanger instance
        /// </summary>
        public static TaskManager Instance
        {
            get
            {
                return _taskManager;
            }
        }

        /// <summary>
        /// Gets a list of task threads of this task manager
        /// </summary>
        public IList<TaskThread> TaskThreads
        {
            get
            {
                return new ReadOnlyCollection<TaskThread>(this._taskThreads);
            }
        }
    }
}