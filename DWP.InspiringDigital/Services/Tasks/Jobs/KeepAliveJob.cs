﻿using System.Net;

namespace DWP.InspiringDigital.Services.Tasks.Jobs
{
    public class KeepAliveJob : ITask
    {
        public void Execute(System.Xml.XmlNode node)
        {
            if (node.Attributes["pingUrl"] != null)
            {
                string pingUrl = node.Attributes["pingUrl"].Value;
                using (var wc = new WebClient())
                {
                   // LogHelper.LogMessage("KeepAlive", string.Format("pinging {0}", pingUrl), "");

                    wc.DownloadString(pingUrl);
                }
            }
        }
    }
}
