﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Services;

namespace DWP.InspiringDigital.Services.Tasks.Jobs
{
    public class ProcessEmailQueueJob : ITask
    {
        public void Execute(System.Xml.XmlNode node)
        {
            ILogService logService = App_Start.NinjectWebCommon.Resolve<ILogService>();
            IEmailService emailService = App_Start.NinjectWebCommon.Resolve<IEmailService>();

            logService.LogMessage("ProcessEmailQueueJob", "Started", string.Empty);

            emailService.ProcessUnsentEmails();
        }
    }
}