﻿using DWP.InspiringDigital.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Services
{
    public interface IActivityService
    {
        IList<Activity> GetActivities();
        IList<Activity> GetActivities(ActivityType type);
        Activity GetActivity(int id);
        Activity GetActivity(string title);
    }
}
