﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Xsl;

using DWP.InspiringDigital.Models;
using DWP.InspiringDigital.Services;
using DWP.InspiringDigital.Dal;
using DWP.InspiringDigital.Utility;

namespace DWP.InspiringDigital.Services
{
    public class EmailService : IEmailService
    {
        IGenricRepository<Email> _emailRepository;

        public EmailService(IGenricRepository<Email> emailRepository)
        {
            _emailRepository = emailRepository;
        }

        public void SendEmail(string fromAddress, string toAddress, string ccAddresses, string subject, string xml, string xsltPath)
        {

            XmlReader xr = XmlReader.Create(new StringReader(xml));

            // Load the style sheet.
            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(HostingEnvironment.MapPath(xsltPath));

            // Transform the file and output an HTML string. 
            string HTMLoutput;
            StringWriter writer = new StringWriter();
            xslt.Transform(xr, null, writer);
            string body = writer.ToString();
            writer.Close();


            try
            {

                SendEmail(fromAddress, toAddress, ccAddresses, subject, body);
            }
            catch
            {
            }
        }

        public void SendEmail(string fromAddress, string toAddress, string ccAddresses, string subject, string body)
        {
            
                Email email = new Email
                {
                    FromAddress = fromAddress,
                    ToAddress = toAddress,
                    CCAddresses = ccAddresses,
                    Subject = subject,
                    Body = body,
                    SendAttempts = 0,
                    CreateDate = DateTime.UtcNow
                };

                //email debug mode, for safety we'll set the original send address (rather than swap out at point of sending) to the debug incase after switching off there are unsent test emails in db.
                if (Helpers.GetSettingBool("Email.Debug", true))
                {
                    email.ToAddress = Helpers.GetSettingString("Email.Debug.ToAddress");
                }

                _emailRepository.Add(email);

                try
                {
                    SendEmail(email);
                    email.SendAttempts++;
                    email.SentDate = DateTime.UtcNow;
                    email.SendError = string.Empty;
                }
                catch (Exception ex)
                {
                    email.SendAttempts++;

                    if (ex is SmtpException)
                    {
                        email.SendError = string.Format("Status code : {0} ; Message {1}", ((SmtpException)ex).StatusCode, ((SmtpException)ex).Message);
                    }
                    else
                    {
                        email.SendError = ex.Message;
                    }
                }

                _emailRepository.SaveChanges();
            
        }

        protected void SendEmail(Email email)
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(email.FromAddress);
            mail.To.Add(new MailAddress(email.ToAddress));

            if (!string.IsNullOrEmpty(email.CCAddresses))
            {
                foreach (string address in Helpers.GetListFromString(email.CCAddresses))
                {
                    mail.CC.Add(new MailAddress(address));
                }
            }

            mail.Subject = email.Subject;
            mail.Body = email.Body;
            mail.IsBodyHtml = true;

            new SmtpClient().Send(mail);
        }

        public IEnumerable<Email> GetEmailLog(DateTime startDate, DateTime endDate)
        {
            return _emailRepository.Get(e => e.CreateDate >= startDate && e.CreateDate <= endDate).OrderByDescending(e => e.CreateDate).ToList();
        }

        public void ProcessUnsentEmails()
        {

            int maxSendAttempts = Helpers.GetSettingInt("Email.MaxSendAttempts", 1);
            IEnumerable<Email> unsentEmails = _emailRepository.Get(e => !e.SentDate.HasValue && e.SendAttempts < maxSendAttempts).ToList();

            foreach (Email email in unsentEmails)
            {
                try
                {
                    this.SendEmail(email);
                    email.SendAttempts++;
                    email.SentDate = DateTime.UtcNow;
                    email.SendError = string.Empty;
                }
                catch (Exception ex)
                {
                  
                    email.SendAttempts++;

                    if (ex is SmtpException)
                    {
                        email.SendError = string.Format("Status code : {0} ; Message {1}", ((SmtpException)ex).StatusCode, ((SmtpException)ex).Message);
                    }
                    else
                    {
                        email.SendError = ex.Message;
                    }
                }

                _emailRepository.Update(email);
            }

        }


    }
}