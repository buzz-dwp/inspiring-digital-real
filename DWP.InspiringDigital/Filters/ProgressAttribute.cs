﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models
{
    [System.AttributeUsage(System.AttributeTargets.Property |System.AttributeTargets.Struct)]
    public class Progress : System.Attribute
    {
        private int _completionValue;
        private int _order;
        private string _display;

        public int CompletionValue{ 
            get{ return _completionValue; }
        }

        public int Order {
            get { return _order; }
        }

        public string Display {
            get { return _display; }
        }


        public Progress(int order, int completionValue, string display)
        {
            _completionValue = completionValue;
            _order = order;
            _display = display;
        }
    }
}