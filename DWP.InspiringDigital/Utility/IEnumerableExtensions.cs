﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;

using DWP.InspiringDigital.Utility;

namespace DWP.InspiringDigital.Utility
{
    public static class IEnumerableExtensions
    {
        public static DataTable ToDataTable<T>(this List<T> items, string title)
        {
            var tb = new DataTable(title);

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                tb.Columns.Add(Helpers.GetDisplayName(typeof(T), prop.Name), prop.PropertyType);
            }

            foreach (var item in items)
            {
                var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(values);
            }

            return tb;
        }

    }
}