﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

/// <summary>
/// Summary description for Image
/// </summary>
/// 

namespace DWP.InspiringDigital.Utility
{

    public enum ResizeType
        {
            Crop,
            Pad
        }
        public enum AnchorY
        {
            Middle,
            Top,
            Bottom
        }
        public enum AnchorX
        {
            Center,
            Left,
            Right
        }

    public class ResizeImage
    {

      

        private string _imageCachePath;
        private string _backgroundColor;

        private int _newWidth = -1;
        private int _newHeight = -1;


        enum Dimensions
        {
            Width,
            Height
        }
        enum AnchorPosition
        {
            Top,
            Center,
            Bottom,
            Left,
            Right
        }

        public ResizeImage(Bitmap ImageSrc)
        {

        }


        //function to check maximal sizes, if image exceeds them then it is resized, else it is left as is and return false
        public static bool EnsureMaximalSizes(string physicalFilePath, int maxWidth, int maxHeight, int jpgQuality )
        {
            System.Drawing.Image originalImg = System.Drawing.Image.FromFile(physicalFilePath);

            if (originalImg.Width > maxWidth || originalImg.Height > maxHeight)
            {
                System.Drawing.Image resizedImg;

                decimal hScale = (decimal)maxWidth / originalImg.Width;
                decimal vScale = (decimal)maxHeight / originalImg.Height;
                decimal resizePercentage = 100;

                if (hScale <= vScale)
                    resizePercentage = hScale * 100;
                else
                    resizePercentage = vScale * 100;

                resizedImg = ScaleByPercent(originalImg, resizePercentage);

                originalImg.Dispose();

                ImageCodecInfo myImageCodecInfo;
                myImageCodecInfo = GetEncoderInfo("image/jpeg");

                System.Drawing.Imaging.Encoder myEncoder;
                EncoderParameter myEncoderParameter;
                EncoderParameters myEncoderParameters;

                myEncoder = System.Drawing.Imaging.Encoder.Quality;
                myEncoderParameters = new EncoderParameters(1);
                myEncoderParameter = new EncoderParameter(myEncoder, jpgQuality);
                myEncoderParameters.Param[0] = myEncoderParameter;

                resizedImg.Save(physicalFilePath, myImageCodecInfo, myEncoderParameters);
                resizedImg.Dispose();

                return true;
            }
            else
            {
                originalImg.Dispose();
                return false;
            }
        }

        public static System.Drawing.Image ScaleByWidth(System.Drawing.Image imgPhoto, int newWidth)
        {
            return ScaleByPercent(imgPhoto, ((decimal)newWidth / (decimal)imgPhoto.Width) * 100);
        }

        public static System.Drawing.Image ScaleByHeight(System.Drawing.Image imgPhoto, int newHeight)
        {
            return ScaleByPercent(imgPhoto, ((decimal)newHeight / (decimal)imgPhoto.Height) * 100);
        }

        public static System.Drawing.Image ScaleByPercent(System.Drawing.Image imgPhoto, decimal Percent)
        {
            //quick fix to stop upscaling


            decimal nPercent = (Percent / 100);

            if (nPercent > 1)
                nPercent = 1;

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;

            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            //Graphics objects can not be created from bitmaps with an Indexed Pixel Format, use RGB instead.
            PixelFormat pixelFormat = imgPhoto.PixelFormat;
            if (pixelFormat.ToString().Contains("Indexed") || pixelFormat.ToString() == "8207")
                pixelFormat = PixelFormat.Format24bppRgb;

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                                     pixelFormat);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Transparent);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static System.Drawing.Image FixedSize(System.Drawing.Image imgPhoto, int Width, int Height)
        {
            return FixedSize(imgPhoto, Width, Height, null);

        }

        public static System.Drawing.Image FixedSize(System.Drawing.Image imgPhoto, int Width, int Height, string backgroundColor)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            Color destColor = Color.Transparent;

            decimal nPercent = 0;
            decimal nPercentW = 0;
            decimal nPercentH = 0;

            nPercentW = ((decimal)Width / (decimal)sourceWidth);
            nPercentH = ((decimal)Height / (decimal)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            if (!string.IsNullOrEmpty(backgroundColor))
                destColor = ColorTranslator.FromHtml(backgroundColor);
     


            //Graphics objects can not be created from bitmaps with an Indexed Pixel Format, use RGB instead.
            PixelFormat pixelFormat = imgPhoto.PixelFormat;
            if (pixelFormat.ToString().Contains("Indexed") || pixelFormat.ToString() == "8207")
                pixelFormat = PixelFormat.Format24bppRgb;

            Bitmap bmPhoto = new Bitmap(Width, Height,
                              pixelFormat);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                             imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(destColor);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static System.Drawing.Image Crop(System.Drawing.Image imgPhoto, int Width,
                    int Height, AnchorX anchorX, AnchorY anchorY)
        {
            return Crop(imgPhoto, Width, Height, anchorX, anchorY, false);
        }

        public static System.Drawing.Image Crop(System.Drawing.Image imgPhoto, int Width,
                    int Height, AnchorX anchorX, AnchorY anchorY, bool allowUpscaling)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            decimal nPercent = 0;
            decimal nPercentW = 0;
            decimal nPercentH = 0;

            nPercentW = ((decimal)Width / (decimal)sourceWidth);
            nPercentH = ((decimal)Height / (decimal)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                switch (anchorY)
                {
                    case AnchorY.Top:
                        destY = 0;
                        break;
                    case AnchorY.Bottom:
                        destY = (int)
                            (Height - (sourceHeight * nPercent));
                        break;
                    default:
                        destY = (int)
                            ((Height - (sourceHeight * nPercent)) / 2);
                        break;
                }
            }
            else
            {
                nPercent = nPercentH;
                switch (anchorX)
                {
                    case AnchorX.Left:
                        destX = 0;
                        break;
                    case AnchorX.Right:
                        destX = (int)
                          (Width - (sourceWidth * nPercent));
                        break;
                    default:
                        destX = (int)
                          ((Width - (sourceWidth * nPercent)) / 2);
                        break;
                }
            }

            //quick fix to stop upscaling
             if (nPercent > 1 && !allowUpscaling)
                 nPercent = 1;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);


            //Graphics objects can not be created from bitmaps with an Indexed Pixel Format, use RGB instead.
            PixelFormat pixelFormat = imgPhoto.PixelFormat;
            if (pixelFormat.ToString().Contains("Indexed") || pixelFormat.ToString() == "8207")
                pixelFormat = PixelFormat.Format24bppRgb;

            Bitmap bmPhoto = new Bitmap(Width,
                    Height, pixelFormat);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Transparent);
         //   grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }


        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        public static string GetMimeType(Image i)
        {
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
            {
                if (codec.FormatID == i.RawFormat.Guid)
                    return codec.MimeType;
            }

            return "image/unknown";
        }


    }

}
