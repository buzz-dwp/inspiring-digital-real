﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Reflection;
using AutoMapper;

using System.IO;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DWP.InspiringDigital.Utility
{
    public static class Helpers
    {

        
            public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
            {
                foreach (T item in source)
                    action(item);
            }
        

        public static List<string> GetListFromString(string input)
        {
            return input.Split(new char[]{',',';'}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static string GetSettingString(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public static List<string> GetSettingStringList(string key)
        {
            return GetListFromString(ConfigurationManager.AppSettings[key].ToString());
        }

        public static int GetSettingInt(string key)
        {
            return GetSettingInt(key, -1);
        }

        public static int GetSettingInt(string key, int defaultValue)
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]))
                return Convert.ToInt32(ConfigurationManager.AppSettings[key].ToString());
            else
                return defaultValue;
        }

        public static bool GetSettingBool(string key, bool defaultValue)
        {
            bool ret = defaultValue;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]))
            {
                Boolean.TryParse(ConfigurationManager.AppSettings[key], out ret);
            }

            return ret;
        }

        public static string GetStringFromList(List<string> items)
        {
            return GetStringFromList(items, ";");
        }

        public static string GetStringFromList(List<string> items, string seperator)
        {
            string ret = string.Empty;
            int count = 0;

            foreach (string item in items)
            {
                ret += string.Format("{0}{1}", (count != 0) ? seperator : string.Empty, item);
                count++;
            }

            return ret;
        }

        public static SelectList ToSelectList(this Enum enumeration)
        {
            var list = (from Enum d in Enum.GetValues(enumeration.GetType())
                        select new { ID = (int)Enum.Parse(enumeration.GetType(), Enum.GetName(enumeration.GetType(), d)), Name = d.ToString().Replace("_", " ").Replace("__","-")}).ToList();
            return new SelectList(list, "ID", "Name", (int)Enum.Parse(enumeration.GetType(), Enum.GetName(enumeration.GetType(), enumeration)));
        }

        public static string FilePathEncode(this string input)
        {
            string safeName = input;
            char[] invalidCharacters = Path.GetInvalidPathChars();

            foreach (char c in invalidCharacters)
            {
                safeName = safeName.Replace(c, '-');
            }

            return safeName;
        }

        public static string GetMimeType(string extension)
        {
            string fileExtension = extension;

            if (fileExtension.IndexOf('.') > -1)
                fileExtension = fileExtension.Substring(fileExtension.LastIndexOf(".") + 1);

            string mimeType = "application/octet-stream";

            switch (fileExtension)
            {
                case "mp3": mimeType = "audio/mpeg"; break;
                case "ogg": mimeType = "audio/ogg"; break;
                case "jpg": mimeType = "image/jpeg"; break;
                case "png": mimeType = "image/png"; break;
                case "flv": mimeType = "video/flv"; break;
                case "mp4": mimeType = "video/mp4"; break;
                case "pdf": mimeType = "application/pdf"; break;
            }

            return mimeType;
        }

        public static void UpdateEntityWithViewModel<TViewModel, TEntity>(TViewModel viewModel, TEntity entity)
        {
            Mapper.CreateMap<TViewModel, TEntity>();
            Mapper.Map((TViewModel)viewModel, (TEntity)entity);
        }

        public static TViewModel ConvertEntityToViewModel<TEntity, TViewModel>(TEntity entity)
        {
            Mapper.CreateMap<TEntity, TViewModel>();
            return Mapper.Map<TEntity, TViewModel>(entity);
        }

        public static TEntity ConvertViewModelToEntity<TViewModel, TEntity>(TViewModel viewModel)
        {
            Mapper.CreateMap<TViewModel,TEntity>();
            return Mapper.Map<TViewModel, TEntity>(viewModel);
        }


        public static string GetDisplayName(Type type, string propertyName)
        {

            //Type type = typeof(TModel);

            // First look into attributes on a type and it's parents
            DisplayAttribute attr;
            attr = (DisplayAttribute)type.GetProperty(propertyName).GetCustomAttributes(typeof(DisplayAttribute), true).SingleOrDefault();

            // Look for [MetadataType] attribute in type hierarchy
            // http://stackoverflow.com/questions/1910532/attribute-isdefined-doesnt-see-attributes-applied-with-metadatatype-class
            if (attr == null)
            {
                MetadataTypeAttribute metadataType = (MetadataTypeAttribute)type.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                if (metadataType != null)
                {
                    var property = metadataType.MetadataClassType.GetProperty(propertyName);
                    if (property != null)
                    {
                        attr = (DisplayAttribute)property.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();
                    }
                }
            }
            return (attr != null) ? attr.Name : propertyName;


        }

    }
}