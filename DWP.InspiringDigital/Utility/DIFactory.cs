﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;

using Ninject.Modules;
using Ninject.Web.Common;
using System.Reflection;

using DWP.InspiringDigital.App_Start;

namespace DWP.InspiringDigital.Utility
{
    public static class DIFactory
    {

     
        public static T Resolve<T>()
        {
           return NinjectWebCommon.GetKernel().Get<T>();  
        }

        public static T Resolve<T>(string name)
        {
            return NinjectWebCommon.GetKernel().Get<T>(name);
        }



     


    }
}