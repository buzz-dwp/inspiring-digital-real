﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:import href="Master.xslt"/>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template name="content">
      <div class="text-center">
        
        <img src="http://sidekick.buzzinteractive.co.uk/content/images/sidekick/logo-home.png" alt="Sidekick" />
        
        


        <table align="center" width="600" border="0" cellpadding="0" cellspacing="0" style="margin:auto;">
      
          <tr>
            <td>
              <img src="http://sidekick.buzzinteractive.co.uk/content/images/sidekick/superman-masked.png" alt="Superman" />
            </td>
            <td>
              <h1>Your Sidekick is here to help you reset your password.</h1>
              <p>
                Visit the link below to quickly get going again.
              </p>
              <p><a href="{ForgottenPasswordEmailViewModel/ResetUrl}">
                  <xsl:value-of select="ForgottenPasswordEmailViewModel/ResetUrl"/>
                </a>
              </p>

            </td>
          </tr>

        </table>

      
      </div>
    </xsl:template>
    
</xsl:stylesheet>
