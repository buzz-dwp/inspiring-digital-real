﻿<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:variable name="SiteUrl" >http://myaccount.gmgpsg.com</xsl:variable>
  
  <xsl:template name="formatDate">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring-before($date, '-')" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />
    <xsl:value-of select="concat ( $day, '/', $month, '/', $year)" />
  </xsl:template>
  
  <xsl:template match="/">
        <html>
            <head>
                <meta name="viewport" content="width=device-width" />
                <title>A message from Inspiring Digital</title>
                <style type="text/css">
                  body{
                 /* background-color: #1691BC;*/
                  font-size: 16px;
                  font-family: Arial;
                  }

                  .text-center{
                  text-align: center;
                  }

                  h1,h2,.text-massive
                  {
                  font-size: 24px;
                  }


                  p,li
                  {
                  line-height: 1.4em;
                  }

                  .signature
                  {
                  padding-top: 20px;
                  font-size: 1.2em;
                  }

                  li{
                  margin-bottom: 1em;
                  }

                  p.large
                  {
                  font-size: 2em;
                  }

                  td p.center
                  {
                  text-align: center;
                  }

                  body
                  {
                  }

                </style>
            </head>
            <body>
                <table align="center" width="600" border="0" cellpadding="0" cellspacing="0" style="margin:auto;">
                  <tr>
                    <th>
                      
                    </th>
                  </tr>
                  <tr>
                        <td>
                            <xsl:call-template name="content"  />
                        </td>
                    </tr>
                
                </table>
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>


