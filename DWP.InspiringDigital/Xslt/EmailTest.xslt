﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:import href="Master.xslt"/>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template name="content">

      <p>Congratulations, you&quot;ve got your first email. All emails messages you receive are placed in your &quot;Inbox&quot;, waiting for you to open them.</p>

      <p>Once you have opened an email message you have a choice of options.</p>

      <ul>
        <li>You can do nothing and just keep it, tapping back on the &quot;Inbox&quot; will return you back to your list of messages</li>
        <li>If you don&quot;t want the message you can delete it</li>
        <li>You can send the email message onto someone else so they can read it. This is called &quot;forwarding&quot; and whichever email service you use, there will be a &quot;forward&quot; button. This will ask you for an email address of the person you wish to forward the message to.</li>
        <li>You can &quot;reply&quot; to the message. This is probably the most common feature and allows you to compose a message back to the person who sent the original to you.</li>
      </ul>

      <h2>ACTION</h2>

      <p>Reply to this message now by tapping the REPLY button.</p>

      <p>A message area will appear, allowing you to type your message back to the person who sent the original email. Their email address will be automatically placed in the To: area</p>

      <p>Write a short message saying thank you for the email.</p>

      <p>Once you&quot;re happy, tap the SEND button.</p>

      <p>Doing this will send us back an email and we&quot;ll know you&quot;ve got the hang of replying. Return back to your browser tab showing the Inspiring Digital website and you&quot;ll see how to move onto the next step.</p>

      <p>
        <em>What if I&quot;ve accidently closed the Inspiring Digital browser tab?</em>
      </p>

      <p>Don&quot;t worry, but remember next time use a new tab as you can easily go back to where you left off. Tap the link below to return to the website:</p>

      <p>
        <strong>
          <a href="http://www.inspiringdigital.org/email">Return to Inspiring Digital</a></strong>
      </p>

       
    </xsl:template>
    
</xsl:stylesheet>
