﻿angular.module('inspiringDigital', [])

.controller('EmailActiviyCtrl', ['$scope', '$http', '$timeout',
function ($scope, $http, $timeout) {

    $scope.success = 'not checked';
    $scope.counter = 0;
    $scope.isMonitoring = false;
    $scope.timeoutInterval = 1000;

    $scope.startEmailMonitor = function () {
        $scope.counter = 0;
        $scope.timeoutInterval = 1000;
        $scope.isMonitoring = true;
        $scope.checkEmailFromUser();
     //   $scope.keepCounting();
    };

    $scope.stopMonitoring = function () {
        $scope.isMonitoring = false;
    }

    $scope.incrementCounter = function () {
        $scope.counter++;
        if ($scope.counter == 600) //10mins
        {
            $scope.timeoutInterval = 5000;
        }
        else if ($scope.counter == 720) //20mins
        {
            $scope.stopMonitoring();
        }

    }
   
    $scope.checkEmailFromUser = function(){

        if ($scope.isMonitoring) {
            $scope.incrementCounter();

            $http.post('/email/checkemail').success(function (data) {

                $scope.success = data.success;

                if ($scope.success == true) {
                    $('#nextstep-form').submit();
                }
                else {
                    $timeout($scope.checkEmailFromUser, $scope.timeoutInterval);
                }

            }).error(function(data, status, headers, config) {
                    $timeout($scope.checkEmailFromUser, $scope.timeoutInterval);
            });;
        }
    }

    $scope.startEmailMonitor();
}])


.controller('EmailFormCtrl', ['$scope', '$http',
function ($scope, $http) {

    $scope.submitForm = function () {

    
        if ($scope.emailForm.$valid) {
            $scope.showErrors = false;
            var postData = {};
            postData.email = $scope.email;

            $http.post('/email/updateemailasync', postData).success(function (data) {

                if (data.errors) {
                    $scope.errors = data.errors;
                }
                else {


                    //used in 2 place one in a modal where page should be reloaded
                    //the other if successful then should post to next step
                    if ($('#email-modal').length && $('#email-modal').length > 0) {
                        window.location.reload();
                    }
                    else {
                        $('#send-email-form').submit();
                    }
                }


            });
        }
        else {
            $scope.showErrors = true;
        }
    };
  
}]);


