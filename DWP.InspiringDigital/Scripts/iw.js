﻿
function equalizeColumns(){

    var groups = {};
    $('*[data-equalize]').each(function () {
        groups[$(this).attr('data-equalize')] = true;
    });


    for (var i in groups) {
        $('*[data-equalize=' + i + ']').equalHeightColumns({
            minWidth: 767
        });
    }

}

equalizeColumns();

$('*[data-hint-text]').focus(function () {

    $('.register-hint').addClass('hidden');

    var hint = $($(this).attr('data-hint-text'));
    if (hint != null) {
        hint.removeClass('hidden');
    }

});