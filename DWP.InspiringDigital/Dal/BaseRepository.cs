﻿using System;
using System.Data;
using System.Data.Entity;

namespace DWP.InspiringDigital.Dal
{
    public class BaseRepository<T> where T : class, new()
    {
        protected IUnitOfWork UnitOfWork { get; set; }
        protected DbSet<T> dbSet { get; set; }


        protected InspiringDigitalDbContext Context
        {
            get { return (InspiringDigitalDbContext)this.UnitOfWork; }
        }

        public BaseRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
            this.UnitOfWork = unitOfWork;

            this.dbSet = this.Context.Set<T>();
        }

        protected virtual DbSet<T> GetDbSet<T>() where T : class
        {
            return this.Context.Set<T>();
        }

        protected virtual void SetEntityState(object entity, EntityState entityState)
        {
            this.Context.Entry(entity).State = entityState;
        }

        public void SaveChanges()
        {
            this.UnitOfWork.SaveChanges();
        }

    }
}