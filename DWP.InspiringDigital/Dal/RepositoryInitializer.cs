﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;



namespace DWP.InspiringDigital.Dal
{
    public class RepositoryInitializer : IRepositoryInitializer
    {
        private IUnitOfWork unitOfWork;

        public RepositoryInitializer(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            this.unitOfWork = unitOfWork;

        

            Database.SetInitializer(new InspiringDigitalDbContextSeedData());

            //this is to trigger the initialiser, otherwise it wont happen till we try to access something
           // Context.Logs.Add(new Log { ShortMessage = "Initializing", FullMessage = "Initializing", EventDate = DateTime.Now, Source = "RepositoryInitializer" });
            Context.SaveChanges();

        }

        protected InspiringDigitalDbContext Context
        {
            get { return (InspiringDigitalDbContext)this.unitOfWork; }
        }

        public void Initialize()
        {
         
        }

    
    }
}