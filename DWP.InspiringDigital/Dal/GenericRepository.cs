﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data;
using System.Data.Entity;

using DWP.InspiringDigital.Models;
using System.Data.Entity.Infrastructure;

namespace DWP.InspiringDigital.Dal
{
    public class GenericRepository<TEntity> : BaseRepository<TEntity>, IGenricRepository<TEntity> where TEntity : class, new()
    {
        public GenericRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        #region IGenricRepository<TEntity> Members


        public TEntity GetSingle(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
           // return Get(filter, orderBy, includeProperties).SingleOrDefault();
            return Get(filter, orderBy, includeProperties).FirstOrDefault();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", bool includeDeleted = false)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }


            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            IEnumerable<TEntity> ret;

            if (orderBy != null)
            {
                ret = orderBy(query).ToList();
            }
            else
            {
                ret = query.ToList();
            }

            if (!includeDeleted && typeof(TEntity).GetInterfaces().Contains(typeof(IDeletable)))
            {
                ret = ret.Where(o => !((IDeletable)o).DeletedDate.HasValue).ToList();
            }

            return ret;

        }

        //this is probably really bad and breaks all the rules!
        /*
        public IQueryable<TEntity> GetQueriable()
        {
            return dbSet;
        }*/

        public void Add(TEntity entity)
        {
            this.dbSet.Add(entity);
            this.SaveChanges();
        }

        public void Delete(IDeletable entity)
        {
            /*
            if (Context.Entry(entity).State == EntityState.Detached)
                dbSet.Attach((TEntity)entity);
            */

            //we're only going to allow soft deletes today
            entity.DeletedDate = DateTime.UtcNow;
            this.Update((TEntity)entity);
           
            // dbSet.Remove((TEntity)entity);
           // this.SaveChanges();
        }
        /*
        public void Delete(object id)
        {
            TEntity entityToDelete = this.dbSet.Find(id);
            Delete(entityToDelete);
        }*/

        public void Update(TEntity entity)
        {
            this.dbSet.Attach(entity);
            SetEntityState(entity, EntityState.Modified);
            this.SaveChanges();
        }

        #endregion
    }
}