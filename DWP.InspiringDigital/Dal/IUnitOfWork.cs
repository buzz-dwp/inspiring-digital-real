﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWP.InspiringDigital.Dal
{
    /// <summary>
    /// Container for unit of work
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Save changes to unit of work
        /// </summary>
        void SaveChanges();
    }
}
