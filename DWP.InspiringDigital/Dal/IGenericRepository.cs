﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using DWP.InspiringDigital.Models;

namespace DWP.InspiringDigital.Dal
{
    public interface IGenricRepository<TEntity> where TEntity : class , new()
    {

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "", bool includeDeleted = false);
        TEntity GetSingle(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,  string includeProperties = "");
        void Add(TEntity entity);
        void Delete(IDeletable entity);
       /*
        void Delete(object id);
        * */
        void Update(TEntity entity);

        void SaveChanges();
        
        //IQueryable<TEntity> GetQueriable();
    }
}
