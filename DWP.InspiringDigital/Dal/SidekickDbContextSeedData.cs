﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using System.Data.Entity;
using System.Data.Entity.Migrations;

using DWP.InspiringDigital.Models;

namespace DWP.InspiringDigital.Dal
{

     public class InspiringDigitalDbContextSeedData : MigrateDatabaseToLatestVersion<InspiringDigitalDbContext, InspiringDigitalMigrationsConfiguration>
   
    {
        private InspiringDigitalDbContext _context;

        public InspiringDigitalDbContextSeedData()
        {
          
        }
    

        protected void SeedStuff()
        {/*

            _context.SiteProviders.Add(new SiteProvider { Title = "Inspiring Work", BaseUrl = "http://buzzinteractive.dyndns.org:92" });

            var perdev = _context.SupportCategories.Add(new SupportCategory { Title = "Personal Development" });
            var jobtra = _context.SupportCategories.Add(new SupportCategory { Title = "Jobsearch Training" });
            
            _context.SaveChanges();

            var devon =  _context.Areas.Add(new Area { Title = "Devon" });
            var cornwall = _context.Areas.Add(new Area { Title = "Cornwall" });
            var somerset = _context.Areas.Add(new Area { Title = "Somerset" });
            _context.SaveChanges();


            var susie = new SupportProvider {
                SiteProviderId = 1,
                Title = "the susie project",
                ShortDescription = "This programme is for you if you are aged between 18-65, have been in an abusive relationship, and wish to move on with your life. You will join a 14-17 week programme which will increase your confidence and self-esteem.",
                Description = "<p>This programme is for you if you are aged between 18-65, have been in an abusive relationship, and wish to move on with your life. You will join a 14-17 week programme which will increase your confidence and self-esteem.</p>",
                Telephone = "01872 211568",
                Email = "susie@victimsupport.org",
                Website = "www.victimsupport.org"

            };

            _context.SupportProviders.Add(susie);
            _context.SaveChanges();

            susie.Areas.Add(devon);
            susie.Areas.Add(cornwall);

            susie.SupportCategories.Add(perdev);
            susie.SupportCategories.Add(jobtra);
            _context.SaveChanges();
            

            var cornwallWorks = new SupportProvider {
                SiteProviderId = 1,
                Title = "cornwall works with families",
                ShortDescription = "This is for you if you are part of a family where no-one is working and you would like to work and increase your family income. You might be a mum, dad, son or daughter. If you’re over 16 and someone in your immediate or extended family is receiving a benefit from Jobcentre Plus you may be able to benefit from this project.",
                Description = "<p>This is for you if you are part of a family where no-one is working and you would like to work and increase your family income. You might be a mum, dad, son or daughter. If you’re over 16 and someone in your immediate or extended family is receiving a benefit from Jobcentre Plus you may be able to benefit from this project.</p>",
                Telephone = "01872 355015",
                Email = "info@cornwallworks.org.uk",
                Website = "www.cornwallworks.org.uk"

            };

            _context.SupportProviders.Add(cornwallWorks);
            _context.SaveChanges();

            cornwallWorks.Areas.Add(cornwall);

            cornwallWorks.SupportCategories.Add(jobtra);
            cornwallWorks.SupportCategories.Add(perdev);

            _context.SaveChanges();


            var jobcen = new SupportProvider{
                SiteProviderId  = 1,
                Title = "jobcentre plus work clubs",
                ShortDescription = "This if for you if you want informal, helpful and community-based support to help you get back to work. You will meet people and share experiences, find opportunities, make contacts and get support to help you to return to work.",
                Description = "<p>This if for you if you want informal, helpful and community-based support to help you get back to work. You will meet people and share experiences, find opportunities, make contacts and get support to help you to return to work.</p>"
            };

            _context.SupportProviders.Add(jobcen);
            _context.SaveChanges();

            jobcen.Areas.Add(devon);
            jobcen.Areas.Add(cornwall);

            jobcen.SupportCategories.Add(perdev);
            jobcen.SupportCategories.Add(jobtra);

            _context.SaveChanges();
           
            */
        }

       
    }
}