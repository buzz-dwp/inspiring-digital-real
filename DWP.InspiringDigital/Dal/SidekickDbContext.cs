﻿using System.Data.Entity;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Data.Entity.ModelConfiguration;


using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Validation;
using System.Diagnostics;

using DWP.InspiringDigital.Models;

namespace DWP.InspiringDigital.Dal
{
    public class InspiringDigitalDbContext : DbContext, IUnitOfWork
    {
       
        public DbSet<Log> Logs { get; set; }
        public DbSet<Email> Emails { get; set; }
        
        public DbSet<User> Users { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<ProgressRecord> ProgressRecords { get; set; }
        public DbSet<Activity> Activities { get; set; }

        public DbSet<PopMessageRecord> PopMessageRecords { get; set; }

            
            //region set model mappings

            public InspiringDigitalDbContext()
                : base("DefaultConnection")
            {
            

            Configuration.ProxyCreationEnabled = false;
            //    Configuration.LazyLoadingEnabled = false;

            }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                //modelBuilder.Entity<UserProfile>().HasMany(up => up.SupportCategories).WithMany();
                //modelBuilder.Entity<UserProfile>().HasMany(up => up.SupportSpecialities).WithMany();
                //modelBuilder.Entity<ActivityQuestion>().HasMany(aq => aq.ActivityRecordAnswers).WithRequired(ra => ra.ActivityQuestion).WillCascadeOnDelete(false);

                base.OnModelCreating(modelBuilder);
            }

            //region end

            /// <summary>
            /// Save Changes via IUnitOfWork
            /// </summary>
            public new void SaveChanges()
            {
                var changeSet = ChangeTracker.Entries<IAuditable>();
                if (changeSet != null)
                {
                    foreach (var entry in changeSet.Where(c => c.State != EntityState.Unchanged))
                    {
                        entry.Entity.ModifiedDate = DateTime.UtcNow;
                        entry.Entity.ModifiedUser = "";

                        if (entry.State == EntityState.Added)
                        {
                            entry.Entity.CreateDate = DateTime.UtcNow;
                            entry.Entity.CreateUser = "";
                        }
                    }
                }
                 try
                 {

                base.SaveChanges();
                 }
                 catch (DbEntityValidationException dbEx)
                 {
                     foreach (var validationErrors in dbEx.EntityValidationErrors)
                     {
                         foreach (var validationError in validationErrors.ValidationErrors)
                         {
                             Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                         }
                     }
                 }
            }
    }
}