﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using DWP.InspiringDigital.Services;

namespace DWP.InspiringDigital.Hubs
{
    public class ActivityHub : Hub
    {
        private readonly IUserProfileService _userProfileService;

        public ActivityHub(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        public void Hello()
        {
            Clients.All.hello("Test");
        }

        public void SetClientId()
        {
            string id = Context.ConnectionId;
            _userProfileService.SetSignalRId(id);
        }

        public void UpdatePreviousTab()
        {

            var id = _userProfileService.GetSignalRId();
            Clients.Client(id).showPartTwo();
        }
    }
}