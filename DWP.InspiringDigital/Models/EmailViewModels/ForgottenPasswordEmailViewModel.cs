﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models.ViewModels
{
    public class ForgottenPasswordEmailViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ResetUrl { get; set; }
    }

}