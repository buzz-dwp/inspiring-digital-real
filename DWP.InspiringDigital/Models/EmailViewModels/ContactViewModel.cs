﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

using System.ComponentModel.DataAnnotations;

namespace DWP.InspiringDigital.Models.ViewModels
{
    [Serializable]
    public class EmailTestViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}