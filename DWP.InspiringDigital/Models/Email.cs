﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace DWP.InspiringDigital.Models
{
    public class Email
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        public string FromAddress { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        public string ToAddress { get; set; }

        [StringLength(200)]
        public string CCAddresses { get; set; }

        [Required]
        [StringLength(100)]
        public string Subject { get; set; }

        [Required]
        [DataType(DataType.Html)]
        public string Body { get; set; }
        public int SendAttempts { get; set; }
        public string SendError { get; set; }
        public DateTime? SentDate { get; set; }
        public DateTime CreateDate { get; set; }

    }
}