﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models
{
    public class PopMessageRecord
    {
        public string Id { get; set; }
        public string  From { get; set; }
    }
}