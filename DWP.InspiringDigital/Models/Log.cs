﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DWP.InspiringDigital.Models
{
    public class Log
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Source { get; set; }
        public string ShortMessage { get; set; }
        public string FullMessage { get; set; }
        public DateTime EventDate { get; set; }


    }
}