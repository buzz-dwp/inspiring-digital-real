﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models
{
    public enum ActivityType
    {
        Unknown = 0,
        GettingStarted = 10,
        JobHunting = 20
    }

    public class Activity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string FormattedTitle { get; set; }
        public ActivityType Type { get; set; }

        public string Controller { get; set; }

        
    }
}