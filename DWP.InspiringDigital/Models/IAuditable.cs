﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWP.InspiringDigital.Models
{
    public interface IAuditable
    {
        DateTime CreateDate { get; set; }
        string CreateUser { get; set; }
        DateTime ModifiedDate { get; set; }
        string ModifiedUser { get; set; }
    }

    public interface IEntity
    {
        int Id { get; set; }
        
    }

    public interface IDeletable
    { 
        DateTime? DeletedDate { get; set; }
    }
}
