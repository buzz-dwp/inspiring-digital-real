﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models
{
    public class ProgressRecord
    {
        public int Id { get; set; }
        public int UserProfileId { get; set; }
        public int ActivityId { get; set; }
        public int Progress { get; set; }
        public bool IsComplete { get; set; }
        public bool IsChosen { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public virtual Activity Activity { get; set; }
    }
}