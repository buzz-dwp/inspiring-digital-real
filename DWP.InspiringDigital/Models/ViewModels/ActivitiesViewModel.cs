﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models.ViewModels
{
    public class ActivitiesViewModel
    {
        public IList<ActivityViewModel> GettingStartedActivities { get; set; }
        public IList<ActivityViewModel> JobHuntingActivities { get; set; }
    }

    public class ActivityViewModel
    {
        public ActivityViewModel(Activity activity, bool isComplete, bool isChosen)
        {
            Id = activity.Id;
            Name = activity.Name;
            Title = activity.Title;
            FormattedTitle = activity.FormattedTitle;
            Controller = activity.Controller;
            IsComplete = isComplete;
            IsChosen = isChosen;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string FormattedTitle { get; set; }
        public string Controller { get; set; }
        public bool IsComplete { get; set; }
        public bool IsChosen { get; set; }
    }
}