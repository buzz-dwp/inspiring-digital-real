﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWP.InspiringDigital.Models.ViewModels
{
    public class EmailAddressViewModel
    {
        [EmailAddress]
        public string Email { get; set; }
    }
}