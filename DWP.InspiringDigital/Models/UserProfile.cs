﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection;
using DWP.InspiringDigital.Services;
using DWP.InspiringDigital.Dal;


namespace DWP.InspiringDigital.Models
{
    public enum Gender
    { 
        All = 0,
        Male = 1,
        Female = 2
    }

    public enum ProfileType
    { 
        Registering = 0,
        AnonymousUser = 1,
        User = 2
    }

    public enum ProfileFlags { 
        None,
        ActivitiesComplete,
        ProfileComplete,
        FeedbackUnlocked,
        SupportViewed
    }

    

    public class UserProfile : IAuditable
    {
        public UserProfile()
        {
            //SupportProviderRecords = new List<SupportProviderRecord>();
            //SupportCategories = new List<SupportCategory>();
            //SupportSpecialities = new List<SupportSpeciality>();
//            ProgressRecords = new List<ProgressRecord>();
        }

        public int SiteProviderId { get; set; }

        public int Id { get; set; }

        /// <summary>
        /// Allows us to determin the profiletype without looking up the user and basing it on that
        /// </summary>
        public ProfileType ProfileType { get; set; }

        public string UserName { get; set; }
        public string Name { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }

        public int ProfileCompleted { get; set; }
        public IEnumerable<string> TaskList { get; set; }

        public virtual ICollection<ProgressRecord> ProgressRecords { get; set; }

        public string SignalRId { get; set; }

        /*
        public int GetProgressForActivity(Activity activity)
        {
            var progress = _userProfileActivityProgressRepository.GetSingle(x => x.ActivityId == activity.Id && x.UserProfileId == Id);
            if (progress == null)
            {
                progress = new UserProfileActivityProgress
                {
                    UserProfileId = Id,
                    ActivityId = activity.Id,
                    Progress = 1
                };
                _userProfileActivityProgressRepository.Add(progress);
            }

            return progress.Progress;
        }*/
    }

   
}