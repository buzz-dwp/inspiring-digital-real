using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;


using System.Collections.Generic;


namespace DWP.InspiringDigital.Dal
{

    public class InspiringDigitalMigrationsConfiguration : DbMigrationsConfiguration<InspiringDigitalDbContext>
    {
        private InspiringDigitalDbContext _context;

        public InspiringDigitalMigrationsConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(InspiringDigitalDbContext context)
        {
            _context = context;
        }

       
    }
}
