﻿using DWP.InspiringDigital.Services.Tasks;
using DWP.InspiringDigital.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Profile;
using System.Web.Routing;

namespace DWP.InspiringDigital
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DBConfig.InitializeDBConfig();
            AuthConfig.RegisterAuth();

            StartTaskManager();
        }

        public void Profile_OnMigrateAnonymous(object sender, ProfileMigrateEventArgs args)
        {
            System.Web.Security.AnonymousIdentificationModule.ClearAnonymousIdentifier();
        }
        
        public void StartTaskManager()
        {
            TaskManager.Instance.Initialize(Server.MapPath(Helpers.GetSettingString("Tasks.ConfigFilePath")), "TasksConfig");
            TaskManager.Instance.Start();
        }
    }
}